import os
import re
import subprocess

docker_image = 'bitbucket-build-statistics:' + os.getenv('BITBUCKET_BUILD_NUMBER', 'local')


def docker_build():
    """
    Build the docker image for tests.
    :return:
    """
    args = [
        'docker',
        'build',
        '-t',
        docker_image,
        '.',
    ]
    subprocess.run(args, check=True)


def setup_module():
    docker_build()


def test_no_username_parameter():
    args = [
        'docker',
        'run',
        '-e', 'BITBUCKET_APP_PASSWORD=test_password',
        '-e', 'WORKSPACE=test_workspace',
        '-e', 'REPO_LIST=test_repo_list',
        '-e', 'DEBUG=True',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 1
    assert '✖ Validation errors: \nBITBUCKET_USERNAME:\n- required field' in result.stdout


def test_no_password_parameter():
    args = [
        'docker',
        'run',
        '-e', 'BITBUCKET_USERNAME=test_user_name',
        '-e', 'WORKSPACE=test_workspace',
        '-e', 'REPO_LIST=test_repo_list',
        '-e', 'DEBUG=True',
        docker_image,
    ]

    result = subprocess.run(args, check=False, text=True, capture_output=True)
    assert result.returncode == 1
    assert '✖ Validation errors: \nBITBUCKET_APP_PASSWORD:\n- required field' in result.stdout


def test_success():
    test_user_name = os.getenv('BITBUCKET_USERNAME', 'local')
    test_password = os.getenv('BITBUCKET_APP_PASSWORD', 'local')
    test_workspace = os.getenv('WORKSPACE', 'local')
    test_repo_list = os.getenv('REPO_LIST', 'local')

    args = [
        'docker',
        'run',
        '-e', 'BITBUCKET_USERNAME=' + test_user_name,
        '-e', 'BITBUCKET_APP_PASSWORD=' + test_password,
        '-e', 'WORKSPACE=' + test_workspace,
        '-e', 'REPO_LIST=' + test_repo_list,
        docker_image,
    ]

    result = subprocess.run(args, check=True, text=True, capture_output=True)

    assert result.returncode == 0
    assert 'Bitbucket build statistics' in result.stdout
    assert '✔ Success!' in result.stdout
    assert 'Builds | Build Duration |' in result.stdout
    assert re.search(r'\d+d \d+h \d+m \d+s', result.stdout) is not None
    print(result.stdout)
